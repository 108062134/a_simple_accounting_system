import sys
cur_money = 0

def read_expense():
    str = input("Add an expense or income record with description and amount:")
    strs = str.split(' ')
    global cur_money
    cur_money += int(strs[1])
    print(cur_money)

if __name__ == '__main__':
    round = 1
    money = input("How much money do you have?")
    cur_money = int(money)
    if len(sys.argv) > 1:
        round = int(sys.argv[1])
    while round > 0:
        read_expense()
        round -= 1